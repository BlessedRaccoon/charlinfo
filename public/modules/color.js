/**
 * Log a message with a color
 * @param {string} message Message to print
 * @param {string} color color to display (black, red, green, yellow, blue, magenta, cyan, white)
 */
function logcolor(message, color = "white") {
    const colors = {
        black: `\x1b[30m`,
        red: `\x1b[31m`,
        green: `\x1b[32m`,
        yellow: `\x1b[33m`,
        blue: `\x1b[34m`,
        magenta: `\x1b[35m`,
        cyan: `\x1b[36m`,
        white: `\x1b[37m`,
    };
    if(colors[color] !== undefined) {
        console.log(`${colors[color]}%s${reset()}`, message);
    }
    else {
        console.log(`${colors.white}%s${reset()}`, message);
    }
}

function reset() {
    return "\033[0m"
}

/**
 * Print a warning
 * @param {string} message message to print
 */
function logwarning(message = "Cela ne devrait pas arriver") {
    logcolor(`WARNING : ${message}`, "jaune");
}

/**
 * Print an error
 * @param {string} message message to print
 */
function logerror(message = "Il y a eu une erreur") {
    logcolor(`ERROR : ${message}`, "rouge");
}

exports.logcolor = logcolor;
exports.logwarning = logwarning;
exports.logerror = logerror;