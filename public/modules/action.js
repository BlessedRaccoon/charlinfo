const fs = require('fs');
const path = require('path');
var fileUpload = require('express-fileupload');
const {logcolor} = require("./color.js");

const DATE = require("./date.js");

// --------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui supprime un dossier.
 * @param chemin Chemin relatif (sans le /) depuis le haut de l'arborescence du dossier à supprimer
 */
exports.supprimerDossier = function(chemin){
    try{
        let dossier = fs.readdirSync(path.join(__dirname, `../../${chemin}`)); // On lit le dossier du cours

        for(let fichier in dossier){
            fs.unlinkSync(path.join(__dirname, `../../${chemin}`, dossier[fichier])); // On supprime tous les fichiers du dossier
        }

        fs.rmdirSync(path.join(__dirname, `../../${chemin}`)); // On suprrime le dossier du cours

        return true;
    }
    catch (error) {
        logerror(`Suppression du dossier échouée :\n${error}`, "jaune");
        return false;
    }
}




// ----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui supprime un fichier.
 * @param chemin Chemin absolu (sans le /) depuis le haut de l'arborescence du fichier à supprimer
 */
exports.supprimerFichier = function(chemin){
    fs.unlinkSync(path.join(__dirname, `../../${chemin}`));
}




// -----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui, à un fichier JSON, supprime un élément (non objet) d'un tableau
 * @param chemin Chemin absolu (sans le /) depuis le haut de l'arborescence du fichier JSON
 * @param cheminTableau Elements du chemin (sous forme de tableau) qui constituent le chemin complet jusqu'au tableau
 * @param element Element du tableau à supprimer
 * @returns Vrai si l'élément a bien été supprimé
 */
exports.supprimerElementTableauJSON = function(chemin, cheminTableau, element){
    let fichier = require(`../../${chemin}`);
    let chem = getCheminJSON(cheminTableau, fichier); // On concatène le chemin

    if(chem.indexOf(element) == -1) return false;

    chem.splice(chem.indexOf(element), 1); // On supprime le slug du cours
    fs.writeFileSync(path.join(__dirname, `../../${chemin}`), JSON.stringify(fichier)); // On sauvegarde
    return true;
}




// -----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui vient concaténer le chemin d'un élément d'un JSON
 * @param cheminTableau Elements du chemin (sous forme de tableau) qui constituent le chemin complet jusqu'au tableau
 * @param fich Fichier JSON
 * @returns Le chemin concaténé
 */
var getCheminJSON = function(cheminTableau, fich){
    let chemin = fich;
    for(let index in cheminTableau){
        chemin = chemin[cheminTableau[index]];
    }
    return chemin;
}




// -----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui supprime un objet d'un tableau JSON
 * @param path Chemin absolu (sans le /) depuis le haut de l'arborescence du fichier JSON
 * @param cheminTableau Elements du chemin (sous forme de tableau) qui constituent le chemin complet jusqu'au tableau
 * @param nomComparer Nom de l'élément dans l'objet à rechercher
 * @param resultat Résultat que doit donner l'élément
 * @returns vrai si l'objet a pu être supprimé
 */
exports.supprimerObjetTableauJSON = function(path, cheminTableau, nomComparer, resultat){
    let fichier = require("../../"+path);
    let chemin = getCheminJSON(cheminTableau, fichier); // On concatène le chemin

    let trouve = false;

    for(let ind in chemin){ // On parcours tous les objets du tableau
        if(chemin[ind][nomComparer] == resultat){ // On recherche le résultat que l'on souhaite sur la propriété voulue
            chemin.splice(ind, 1); // Supprime l'élément recherché
            trouve = true; // L'opération s'est bien passée
        }
    }

    fs.writeFileSync(path, JSON.stringify(fichier)); // On sauvegarde
    return trouve; // L'élément n'a pas été trouvé
}




// ---------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction pour enregistrer un élément (ou un objet) dans un TABLEAU JSON
 * @param obj Element (ou objet) à sauvegarder
 * @param path Chemin absolu (sans le /) depuis le haut de l'arborescence du fichier JSON
 * @param cheminTableau Elements du chemin (sous forme de tableau) qui constituent le chemin complet jusqu'au tableau
 */
exports.enregistrerElementDansTableauJSON = function(obj, path, cheminTableau){
    try{
        let fichier = require("../../"+path);
        let chemin = getCheminJSON(cheminTableau, fichier);
        chemin.push(obj);
        fs.writeFileSync(path, JSON.stringify(fichier));
        return true;
    }
    catch (error) {
        return false;
    }
}




// ----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction pour enregistrer un élément (ou un objet) EN DEBUT d'un un TABLEAU JSON
 * @param obj Element (ou objet) à sauvegarder
 * @param path Chemin absolu (sans le /) depuis le haut de l'arborescence du fichier JSON
 * @param cheminTableau Elements du chemin (sous forme de tableau) qui constituent le chemin complet jusqu'au tableau
 */
exports.enregistrerElementDansDebutTableauJSON = function(obj, path, cheminTableau){
    try{
        let fichier = require(path);
        let chemin = getCheminJSON(cheminTableau, fichier);
        chemin.unshift(obj);
        fs.writeFileSync(path, JSON.stringify(fichier));
        return true;
    }
    catch (error) {
        return false;
    }
}




// ----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui crée un dossier
 * @param chemin Chemin absolu (sans le /) depuis le haut de l'arborescence
 */
exports.ajouterDossier = function(chemin){
    fs.mkdirSync(path.join(__dirname, `../../${chemin}`));
}




// ---------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui crée un fichier
 * @param chemin Chemin absolu (sans le /) depuis le haut de l'arborescence
 * @param contenu Contenu du fichier
 */
exports.ajouterFichier = function(chemin, contenu){
    fs.writeFileSync(path.join(__dirname, `../../${chemin}`), contenu);
}




// ---------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction pour enregistrer un élément (ou un objet) dans un OBJET JSON
 * @param nomObj Nom de l'objet à sauvegarder
 * @param contenuObj Contenu de l'objet à sauvegarder
 * @param path Chemin absolu (sans le /) depuis le haut de l'arborescence du fichier JSON
 * @param cheminTableau Elements du chemin (sous forme de tableau) qui constituent le chemin complet jusqu'au tableau
 */
exports.enregistrerElementDansObjetJSON = function(nomObj, contenuObj, path, cheminTableau){
    try{
        let fichier = require("../../"+path);
        let chemin = getCheminJSON(cheminTableau, fichier);
        chemin[nomObj] = contenuObj;
        fs.writeFileSync(path, JSON.stringify(fichier));
        return true;
    }
    catch (error) {
        return false;
    }
}




// ---------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui fusionne deux ensembles
 * @param ensembles Ensembles (Sous forme de TABLEAU) à fusionner
 * @returns ensemble fusionner
 */
exports.fusionnerEnsembles = function(ensembles){
    if (ensembles.length > 1){
        ensembleTotal = {...ensembles[0], ...ensembles[1]};
        i = 2;
        while(i < ensembles.length){
            ensembleTotal = {...ensembleTotal, ...ensembles[i]};
            i++;
        }
        return ensembleTotal;
    }
    else if (ensembles.length == 1){
        return ensembles[0];
    }
    else{
        return -1;
    }
}




// ----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui ajoute les logs d'un utilisateur si ceux-ci n'existent pas déjà
 * @param id ID de l'utilisateur
 */
exports.ajouterLogsUtilisateur = function(id){
    if (! fs.existsSync(path.join(__dirname, "../charlinfo-db/users/"+id))){
        this.ajouterDossier("../charlinfo-db/users/"+id);
    }
    if (! fs.existsSync(path.join(__dirname, "../charlinfo-db/users/"+id+"/logs.json"))){
        obj = {"TYPE": "inscription", "date": DATE.dateISO()};
        this.ajouterFichier("../charlinfo-db/users/"+id+"/logs.json", "{\"logs\":[]}");
        this.enregistrerElementDansTableauJSON(obj, "../charlinfo-db/users/"+id+"/logs.json", ["logs"])
    }
}




// -----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui ajoute les logs d'un utilisateur pour l'upload de fichier
 * @param id ID de l'utilisateur
 * @param req req
 */
exports.LogsUploadFichier = function(id, req){
    obj = {"TYPE": "uploadFichier", "date": DATE.dateISO(), "cours": req.params.cours};
    obj.nom = req.body.titre;
    obj.description = req.body.desc,
    obj.type = req.body.type;
    obj.auteur = req.session.user.id;
    this.enregistrerElementDansTableauJSON(obj, "../charlinfo-db/users/"+id+"/logs.json", ["logs"])
}




// -----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui ajoute les logs d'un utilisateur pour la suppression l'upload de fichier
 * @param id ID de l'utilisateur
 * @param auteur idAuteur du fichier
 * @param req req
 */
exports.LogsSupprimerFichier = function(id, auteur, req){
    obj = {"TYPE": "supprimerFichier", "date": DATE.dateISO(), "cours": req.params.cours, "supprimePar": id};
    this.enregistrerElementDansTableauJSON(obj, "../charlinfo-db/users/"+id+"/logs.json", ["logs"])
    if(auteur !== id) this.enregistrerElementDansTableauJSON(obj, "../charlinfo-db/users/"+auteur+"/logs.json", ["logs"])
    
}




// -----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui sert à administrer les Logs
 * @param id ID de l'utilisateur qui fait les modifs
 * @param user Utilisateur dont on fait des modifications
 * @param nom Nom du TYPE
 */
exports.LogsAdministrer = function(id, user, nom){
    obj = {"TYPE": nom, "date": DATE.dateISO(), "auteur": id, "destinataire": user};
    this.enregistrerElementDansTableauJSON(obj, "../charlinfo-db/users/"+user+"/logs.json", ["logs"])
    obj = {"TYPE": nom, "date": DATE.dateISO(), "auteur": id, "destinataire": user};
    this.enregistrerElementDansTableauJSON(obj, "../charlinfo-db/users/"+id+"/logs.json", ["logs"])
}




// -----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui ajoute dans les logs qu'on a ajouté un utilisateur
 * @param id ID de l'utilisateur qui fait les modifs
 * @param user Utilisateur dont on fait des modifications
 */
exports.LogsAjouterUtilisateur = function(id, user){
    obj = {"TYPE": "ajoutUtilisateur", "date": DATE.dateISO(), "auteur": id, "utilisateurAjoute": user};
    this.enregistrerElementDansTableauJSON(obj, "../charlinfo-db/users/"+id+"/logs.json", ["logs"])
}




// -----------------------------------------------------------------------------------------------------------------------------

/**
 * Fonction qui ajoute dans les logs qu'un cours a été ajouté ou supprimé
 * @param id ID de l'utilisateur qui fait les modifs
 * @param cours nom du cours ajouté
 * @param status "ajoutCours" ou "supprCours"
 */
exports.LogsGererCours = function(id, cours, status){
    obj = {"TYPE": status, "date": DATE.dateISO(), "auteur": id, "coursAjoute": cours};
    this.enregistrerElementDansTableauJSON(obj, "../charlinfo-db/users/"+id+"/logs.json", ["logs"]);
}




// -----------------------------------------------------------------------------------------------------------------------------

