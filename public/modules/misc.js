const fs = require('fs');

/**
 * Fonction qui retourne l'extension d'un fichier
 * @param {string} param Nom du fichier
 * @returns {string} extension du fichier (ou null s'il n'y a pas d'extension)
 */
exports.getExtension = function(param) {
    let ext = (param).split(".");
    return (ext.length > 1) ? ext[ext.length-1] : null
}

/**
 * Fonction qui à un tableau d'entiers retourne le plus petit entier non utilisé de la séquence
 * @param {number[]} arr Tableau d'entiers (trié ou non) dont donner l'entier de la séquence 
 * @returns {number} Plus petit entier non utilisé de la séquence
 */
exports.getSeqFromIntegers = function(arr) {
    if (arr.length == 0) return 0;
    else {
        arr.sort((a, b) => a - b);
        let entAvant = arr[0];
        arr.shift();
        for(let elem of arr){
            if(elem - entAvant !== 1){
                break;
            }
            entAvant = elem;
        }
        entAvant++;
        return entAvant;
    }
}

/**
 * Fonction qui va retourner le plus petit entier libre de la séquence depuis un dossier
 * @param {string} path Chemin qui mène au dossier
 * @returns {number} plus petit entier libre de la séquence
 */
exports.getSeqFromFolder = function(path) {
    let arr = fs.readdirSync(path);
    let fin = [];
    for(let i in arr) {
        if(arr[i] !== "files.json") fin.push(parseInt(arr[i].split(".")[0]));
    }
    let {getSeqFromIntegers} = require("./misc.js")
    return getSeqFromIntegers(fin);
}

exports.verifyIfNotInt = function(result) {
    return isNaN(parseInt(result));
}