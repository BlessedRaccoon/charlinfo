/**
 * Fonction permettant de rediriger l'utilisateur s'il tente d'accéder à un endroit où il n'a pas les droits nécessaires
 * @param {*} req req
 * @param {*} res res
 * @param {string} redirection page de redirection
 * @param {number} grade grade minimum requis
 */
exports.gererPermission = async function(req, res, redirection = "/", grade = 1){
    if(req.session.user === undefined){
        return res.redirect(redirection);
    }
    if(req.session.user.grade < grade){
        return res.redirect(redirection);
    }
}
