/**
 * Fonctions de permissions et de gestion des grades
 */
const PERMISSION = require("../modules/permission.js");

/**
 * Fonctions pratiques qui se chargent de simplifier et d'alléger le code
 */
const ACTION = require("../modules/action.js");

/**
 * Fonctions de gestion des dates
 */
const DATE = require("../modules/date.js");

/**
 * Fonctions nécessaires au chargement de la page
 */
const { charger } = require("../modules/chargerPage.js");
const {
    requete,
    requete_recuperer_version,
    requete_recuperer_changelog
} = require("../modules/requete.js");

const { logcolor, logwarning, logerror } = require("../modules/color.js")
/**
 * Affichage de la charte
 */
exports.charte = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 1);
    await charger([], [], {}, "charte.pug", req, res);
}

/**
 * Construit la page principale
 */
exports.pagePrincipale = async function (req, res) {
    // Permet de charger le changelog
    req.session.changelog = await requete_recuperer_version();
    req.session.changelog = req.session.changelog[0];

    if (req.session.user !== undefined) {
        req.session.sub = undefined;
        let CHANGELOG = await requete_recuperer_changelog();
        for (let index = 0; index < CHANGELOG.length; index++) {
            CHANGELOG[index].liste = await requete("select description from changelog inner join description_changelog dc on changelog.nom = dc.nomchangelog where nomchangelog LIKE $1", [CHANGELOG[index].nom]);
        }
        await charger([CHANGELOG], ["CHANGELOG"], { DATE }, "index.pug", req, res);
    }
    else {
        await charger([], [], { "erreurSub": req.session.sub, "erreurLogin": req.session.login }, "connexion.pug", req, res);
    }
}

/**
 * Déconnecte l'utilisateur
 */
exports.deconnexion = async function (req, res) {
    req.session.user = undefined; // Déconnecte l'utilisateur
    req.session.login = undefined;
    await res.redirect("/");
}

/**
 * Page où l'on affiche les formations
 */
exports.formation = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 1);
    let FORMATION = requete("SELECT id, nom, duree, description FROM Formation ORDER BY nom ASC");
    await charger([FORMATION], ["FORMATION"], {}, "cours.pug", req, res);
}

/**
 * Processus de suppression d'un FICHIER d'un cours
 */
exports.suppressionFichier = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 1);

    if (isNaN(parseInt(req.params.cours)) || isNaN(parseInt(req.params.fichSuppr))) {
        logwarning("Le cours ou le fichier demandé n'a pas été identifié par un entier.");
        return res.redirect("/cours");
    }

    let autFich = await requete("SELECT idauteur, extension FROM Fichier WHERE idCours = $1::integer AND id = $2", [req.params.cours, req.params.fichSuppr]);

    if (autFich.length === 0) {
        logwarning("Le cours ou le fichier demandé n'existe pas.");
        return res.redirect("/cours");
    }

    if (req.session.user.grade >= 3 || autFich[0].idauteur == req.session.user.id) {
        requete("DELETE FROM Fichier WHERE idCours = $1::integer AND id = $2", [req.params.cours, req.params.fichSuppr]);
        ACTION.LogsSupprimerFichier(req.session.user.id, autFich[0].idauteur, req);
        if (autFich[0].extension == undefined) ACTION.supprimerFichier(`../charlinfo-db/cours/${req.params.cours}/${req.params.fichSuppr}`);
        else ACTION.supprimerFichier(`../charlinfo-db/cours/${req.params.cours}/${req.params.fichSuppr}.${autFich[0].extension}`);
        return await res.redirect(`/cours/afficher/${req.params.cours}`);
    }
    else {
        logwarning("L'utilisateur n'a pas les droits nécessaires permettant la suppression du fichier");
        return await res.redirect(`/cours/afficher/${req.params.cours}`);
    }
}

/**
 * Page pour ajouter un fichier dans un cours
 */
exports.pageAjoutFichierCours = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 1);

    if (isNaN(parseInt(req.params.cours))) {
        logwarning("Le cours demandé n'a pas été identifié par un entier.");
        return res.redirect("/cours");
    }

    let reqe = await requete("SELECT Count(*) FROM Cours WHERE id = $1::integer", [req.params.cours]);
    if (reqe[0].count == 0) {
        logwarning("Le cours demandé n'existe pas.");
        return await res.redirect("/cours");
    }
    await charger([], [], { "erreurTaille": req.session.tropGros, "currCours": req.params.cours }, "ajoutFichier.pug", req, res);
}

/**
 * Page où l'on affiche les semestres disponibles pour une formation donnée
 */
exports.semestre = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 1);
    if (isNaN(parseInt(req.params.formation))) {
        logwarning("Le paramètre spécifié n'est pas un entier.");
        return res.redirect("/cours");
    }
    let SEMESTRES = requete("SELECT nom, duree FROM Formation WHERE id = $1", [req.params.formation]);
    await charger([SEMESTRES], ["SEMESTRES"], { "formationId": req.params.formation }, "semestre.pug", req, res)
}

/**
 * Page où l'on affiche les cours disponibles pour une formation donnée à un semestre donné
 */
exports.modules = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 1);

    if (isNaN(parseInt(req.params.formation)) || isNaN(parseInt(req.params.semestre))) {
        logwarning("Le semestre ou la formation spécifiés n'existent pas.");
        return res.redirect("/cours");
    }

    // Ids des cours de la formation et du semestre recherchés
    let reqe = await requete("SELECT id FROM Cours WHERE idFormation = $1::integer AND idSemestre = $2::integer ORDER BY nom ASC", [req.params.formation, req.params.semestre]);

    // Ids, nom, et description des cours recherchés
    let COURS = requete("SELECT id, nom, description, couleur, type, (select count(*) as number from FICHIER where idCours = COURS.id and idFormation = $1::integer and idSemestre = $2::integer) FROM Cours WHERE idFormation = $1::integer AND idSemestre = $2::integer ORDER BY nom ASC", [req.params.formation, req.params.semestre]);
    let obj = { "semestre": req.params.semestre, "formation": req.params.formation };
    obj.PROFS = [];
    // Pour chaque id de cours
    for (let val of reqe) {
        // On recherche les profs associés à ce cours
        let prof = await requete("SELECT idCours, prenom, nom FROM Professeur INNER JOIN Assurer_Cours ON Assurer_Cours.idprof = professeur.id WHERE idCours = $1::integer ORDER BY nom ASC", [val.id]);
        await obj.PROFS.push(prof);
    }
    // Cherche le nom de la formation
    let FORMATION = requete("SELECT nom FROM Formation WHERE id = $1::integer", [req.params.formation]);

    await charger([COURS, FORMATION], ["COURS", "FORMATION"], obj, "modules.pug", req, res);
}

/**
 * Page où l'on ajoute des cours
 */
exports.pageAjoutCours = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 4);
    let PROFS = requete("SELECT id, prenom, nom FROM Professeur");
    let FORMATION = requete("SELECT id, nom, duree FROM Formation");
    await charger([PROFS, FORMATION], ["PROFS", "FORMATION"], {}, "ajoutCours.pug", req, res);
}

/**
 * Page où l'on supprime des cours
 */
exports.pageModifCours = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 4);
    let COURS = requete("SELECT id, nom, type, couleur FROM Cours ORDER BY nom ASC");
    await charger([COURS], ["COURS"], {}, "modifCours.pug", req, res);
}

/**
 * Page où l'on voit les utilisateurs
 */
exports.pageListeUtilisateurs = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 3);
    let USERS = requete("SELECT compte.id, motDePasse, compte.nom, prenom, grade, dateCreation, formation.nom AS nomFormation FROM Compte INNER JOIN Formation ON formation.id = compte.idFormation ORDER BY grade DESC, compte.nom ASC")
    await charger([USERS], ["USERS"], { DATE }, "users.pug", req, res);
}

/**
 * Page où l'on affiche le cours
 */
exports.afficherCours = async function (req, res) {
    req.session.tropGros = undefined;
    await PERMISSION.gererPermission(req, res, "/", 1);

    if (isNaN(parseInt(req.params.cours))) {
        logwarning("Le cours demandé n'est pas identifié avec un entier.");
        return res.redirect("/cours");
    }

    // Vérifie si le cours recherché est bien un cours existant
    let reqe = await requete("SELECT COUNT(*) FROM Cours WHERE id = $1", [req.params.cours]);
    if (reqe[0].count == 0) {
        logwarning("Le cours demandé n'existe pas.");
        return await res.redirect("/cours");
    }

    let COURS = requete("SELECT id, nom FROM Cours WHERE id = $1", [req.params.cours]);
    let temp = await requete("SELECT fichier.id, fichier.nom, compte.nom as nomfamille, datefich, extension, typecours, description, prenom, grade, idauteur FROM Fichier INNER JOIN Compte ON compte.id = fichier.idauteur WHERE idCours = $1::integer ORDER BY datefich ASC", [req.params.cours])
    let PROF = requete("SELECT prenom, nom FROM Assurer_Cours INNER JOIN Professeur ON Professeur.id = Assurer_Cours.idProf WHERE idCours = $1::integer", [req.params.cours]);

    let FICHIER = [{}];
    let NUMBER = 0;

    FICHIER[0].cours = temp.filter(data => data.typecours === 'Cours');
    if (FICHIER[0].cours.length > 0)
        NUMBER++;
    FICHIER[0].exercices = temp.filter(data => data.typecours === 'Exercices');
    if (FICHIER[0].exercices.length > 0)
        NUMBER++;
    FICHIER[0].corrige = temp.filter(data => data.typecours === 'Corrigé');
    if (FICHIER[0].corrige.length > 0)
        NUMBER++;
    FICHIER[0].aide = temp.filter(data => data.typecours === 'Aide');
    if (FICHIER[0].aide.length > 0)
        NUMBER++;
    FICHIER[0].dm = temp.filter(data => data.typecours === 'DM');
    if (FICHIER[0].dm.length > 0)
        NUMBER++;

    FICHIER[0].number = NUMBER;

    await charger([COURS, PROF, FICHIER], ["COURS", "PROF", "FICHIER"], { DATE }, "pageCours.pug", req, res);
}

/**
 * Profil d'un utilisateur
 */
exports.afficherProfil = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/users", 1);

    if (req.session.user.grade >= 2 || req.session.user.id == req.params.user) {
        let reqe = await requete("SELECT id, prenom, nom, grade FROM Compte WHERE id = $1", [req.params.user]);
        if (reqe.length === 0) {
            return await res.redirect('/users');
        }
        await charger([reqe], ["paramUser"], { "LOGS": await require(`../../../charlinfo-db/users/${req.params.user}/logs.json`), DATE }, "user.pug", req, res);
    }
    else {
        await res.redirect("/users");
    }
}

/**
 * Page pour BANNIR un utilisateur
 */
exports.bannir = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/users", 3);

    let reqe = await requete("SELECT id, prenom, nom, grade FROM Compte WHERE id = $1", [req.params.user]);
    if (reqe.length === 1) {
        if (req.session.user.grade >= reqe[0].grade) {
            requete("UPDATE Compte SET motDePasse = 'BANNI' WHERE id = $1", [req.params.user]);
            ACTION.LogsAdministrer(req.session.user.id, reqe.id, "BANNIR");
        }
    }
    else {
        logwarning("L'utilisateur mentionné n'existe pas ! [CELA NE DEVRAIT JAMAIS ARRIVER]");
    }
    await res.redirect("/users");
}

/**
 * Page pour DEBANNIR un utilisateur
 */
exports.debannir = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/users", 3);
    let reqe = await requete("SELECT grade, motDePasse FROM Compte WHERE id = $1", [req.params.user]);

    if (reqe.length === 1) {
        if (req.session.user.grade >= reqe[0].grade && reqe[0].motdepasse === "BANNI") {
            requete("UPDATE Compte SET motDePasse = NULL WHERE id = $1", [req.params.user]);
            ACTION.LogsAdministrer(req.session.user, req.params.user, "DEBANNIR");
        }
    }
    else {
        logwarning("L'utilisateur mentionné n'existe pas ! [CELA NE DEVRAIT JAMAIS ARRIVER]");
    }
    await res.redirect("/users");
}

/**
 * Page pour RETROGRADER un utilisateur
 */
exports.retrograder = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/users", 3);

    let reqe = await requete("SELECT grade, motDePasse FROM Compte WHERE id = $1", [req.params.user]);
    if (reqe.length === 1) {
        if (req.session.user.grade >= reqe[0].grade && reqe[0].grade > 1) {
            requete("UPDATE Compte SET grade = grade-1 WHERE id = $1", [req.params.user])
            ACTION.LogsAdministrer(req.session.user.id, req.params.user, "RETROGRADER")
        }
    }
    else {
        logwarning("L'utilisateur mentionné n'existe pas ! [CELA NE DEVRAIT JAMAIS ARRIVER]");
    }
    await res.redirect("/users");
}

/**
 * Page pour PROMOUVOIR un utilisateur
 */
exports.promouvoir = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/users", 3);

    let reqe = await requete("SELECT grade FROM Compte WHERE id = $1", [req.params.user]);

    if (reqe.length === 1) {
        if ((req.session.user.grade > reqe[0].grade || req.session.user.grade >= 5) && reqe[0].grade < 5) {
            await requete("UPDATE Compte SET grade = grade+1 WHERE id = $1", [req.params.user]);
            ACTION.LogsAdministrer(req.session.user.id, req.params.user, "PROMOUVOIR");
        }
    }
    else {
        logwarning("L'utilisateur mentionné n'existe pas ! [CELA NE DEVRAIT JAMAIS ARRIVER]");
    }
    await res.redirect("/users");
}

/**
 * Page pour SUPPRIMER DEFINITIVEMENT un utilisateur
 */
exports.supprimerUtilisateur = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/users", 5);
    let reqe = await requete("SELECT id FROM Compte WHERE id = $1", [req.params.user]);

    if (reqe.length === 1) {
        ACTION.LogsAdministrer(req.session.user.id, req.params.user, "SUPPRIMER");
        ACTION.supprimerDossier(`../charlinfo-db/users/${req.params.user}`);
        requete("DELETE FROM Compte WHERE id = $1", [req.params.user]);
    }
    else {
        logwarning("L'utilisateur mentionné n'existe pas ! [CELA NE DEVRAIT JAMAIS ARRIVER]");
    }

    await res.redirect("/users");
}

/**
 * Page pour ajouter un Changelog de mise à jour
 */
exports.pageAjoutChangelog = async function (req, res) {
    await PERMISSION.gererPermission(req, res, "/", 5);
    await charger([], [], {}, "changelog.pug", req, res);
}

/**
 * SUPPRESSION d'un log
 */
exports.supprimerLog = async function (req, res) {
    await PERMISSION.gererPermission(req, res, `/users/${req.params.user}`, 4);
    let dateComparer = await require(`../../../charlinfo-db/users/${req.params.user}/logs.json`);
    dateComparer = await dateComparer.logs[req.params.index].date;
    ACTION.supprimerObjetTableauJSON(`../charlinfo-db/users/${req.params.user}/logs.json`, ["logs"], "date", dateComparer);
    await res.redirect(`/users/${req.params.user}`);
}