const {assert} = require("./assert.js");
const {requete} = require("../modules/requete.js");
const {logcolor} = require("../modules/color.js");

const fs = require('fs');

const DATE = require("../modules/date.js");

const {
    charte,
    pagePrincipale,
    deconnexion,
    formation,
    suppressionFichier,
    pageAjoutFichierCours,
    semestre,
    modules,
    pageAjoutCours,
    pageModifCours,
    pageListeUtilisateurs,
    afficherCours,
    afficherProfil,
    bannir,
    debannir,
    retrograder,
    promouvoir,
    supprimerUtilisateur,
    pageAjoutChangelog,
    supprimerLog
} = require("../pages/GET.js");

const {
    connexion,
    inscription,
    ajoutFichierCours,
    creationCours,
    supprimerCours,
    ajoutUtilisateur,
    ajoutChangelog
} = require("../pages/POST.js");

var express = require('express');
var app = express();

// Définit les dossiers "public" et "charlinfo-db" comme statiques (on peut charger du non HTML depuis)
app.use(express.static('..'));
app.use(express.static('../../../charlinfo-db'));

// ------------------------ SETUP --------------------------------- \\
var req = {
    params: {},
    session: {},
    files: {},
    body: {}
}

var res = {
    redirect: (e) => {throw e},
    setHeader: (e1, e2) => {return},
    render: (page, param) => {throw page} 
}
// ------------------------ TESTS --------------------------------- \\

async function tests(){
    let assertEquals = assert(process.argv[2]);

    // INSCRIPTION -------------------------------------------------------------------------------------
    let dat = await DATE.dateISO();
    dat = await dat.split("T");
    dat = await dat.join(" ");
    await requete(`INSERT INTO Compte VALUES ('admin', NULL, NULL, NULL, 1, NULL, '${dat}')`);
    // ----------------------
    req.body.user = "lili";
    req.body.prenom = "jean";
    req.body.nom = "valjean";
    req.body.formation = "DUT Informatique";
    req.body.pass = "carabistouille";
    await inscription(req, res).catch((err) => {
        assertEquals("La fonction devrait être redirigée", "/", err);
        assertEquals("Le message d'erreur n'est pas le bon", "Votre compte n'est pas référencé", req.session.sub);
    })
    req.body.user = 'admin'
    await inscription(req, res).catch((err) => assertEquals("La fonction devrait être redirigée vers la charte", "/charte", err));

    // DECONNEXION -------------------------------------------------------------------------------------
    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    await inscription(req, res).catch((err) => {
        assertEquals("La fonction devrait être redirigée", "/", err);
        assertEquals("Le message d'erreur n'est pas le bon", "Vous avez déjà un compte", req.session.sub);
    });

    // CONNEXION ----------------------------------------------------------------------------------------
    req.body.user = "mimi";
    req.body.prenom = undefined;
    req.body.nom = undefined;
    req.body.formation = undefined;
    req.body.pass = "jeje";
    await connexion(req, res).catch((err) => {
        assertEquals("Le nom d'ulisateur doit être erroné", "Nom d'utilisateur erroné", req.session.login);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    req.body.user = "admin";
    await connexion(req, res).catch((err) => {
        assertEquals("Le mot de passe doit être erroné", "Mot de passe erroné", req.session.login);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    req.body.pass = "carabistouille";
    await connexion(req, res).catch((err) => {
        assertEquals("L'objet user n'est pas correct", "admin", req.session.user.id);
        assertEquals("L'objet user n'est pas correct", "VALJEAN", req.session.user.nom);
        assertEquals("L'objet user n'est pas correct", "Jean", req.session.user.prenom);
        assertEquals("L'objet user n'est pas correct", 1, req.session.user.grade);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });

    // PAGE PRINCIPALE ----------------------------------------------------------------------------------
    await pagePrincipale(req, res).catch((err) => {
        assertEquals("req.session.sub doit être indéfini", undefined, req.session.sub);
        assertEquals("ça doit charger index.pug", "index.pug", err);
    })

    // CHARTE -------------------------------------------------------------------------------------------
    await charte(req, res).catch((err) => {
        assertEquals("La page chargée n'est pas la bonne", "charte.pug", err);
    });

    // FORMATION ----------------------------------------------------------------------------------------
    await formation(req, res).catch((err) => {
        assertEquals("La page chargée n'est pas la bonne", "cours.pug", err);
    });

    await pageListeUtilisateurs(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/", err);
    });

    req.params.user = req.session.user.id;
    await afficherProfil(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "user.pug", err);
    });

    req.params.user = "lilou";
    await afficherProfil(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });

    await afficherProfil(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });

    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });

    await requete("UPDATE Compte SET grade = 3 WHERE id = 'admin'");

    await connexion(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être connecté", "/", err);
    });

    await pageListeUtilisateurs(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "users.pug", err);
    });

    await afficherProfil(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé, car lilou n'existe pas", "/users", err);
    });

    // Insertion de gens -------------------------------------------------------------------------------
    await ajoutUtilisateur(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé, car il n'a pas le grade suffisant", "/users", err);
    });

    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });

    await requete("UPDATE Compte SET grade = 5 WHERE id = 'admin'");

    await connexion(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être connecté", "/", err);
    });

    req.body.id = "admin"
    await ajoutUtilisateur(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé, car l'utilisateur est déjà dans la base de donnée", "/users", err);
        assertEquals("Il devrait y avoir le message d'erreur", `ERREUR : L'utilisateur ${req.body.id} est déjà dans la base de données`, req.session.userAjoute);
    });

    // --------------------------------------------------------------------------------------------------

    req.body.user = "gilbert";
    req.body.prenom = "jean";
    req.body.nom = "valjean";
    req.body.formation = "DUT Informatique";
    req.body.pass = "carabistouille";
    req.body.id = "gilbert"
    await ajoutUtilisateur(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé, car l'utilisateur est déjà dans la base de donnée", "/users", err);
        assertEquals("Il devrait y avoir le message d'insertion reussie", `L'utilisateur ${req.body.id} a été ajouté !`, req.session.userAjoute);
    });
    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    await inscription(req, res).catch((err) => assertEquals("La fonction devrait être redirigée vers la charte", "/charte", err));
    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    req.body.user = "admin";
    await connexion(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être connecté", "/", err);
    });
    
    req.body.user = "hubert";
    req.body.id = "hubert";
    await ajoutUtilisateur(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé, car l'utilisateur est déjà dans la base de donnée", "/users", err);
        assertEquals("Il devrait y avoir le message d'insertion reussie", `L'utilisateur ${req.body.id} a été ajouté !`, req.session.userAjoute);
    });
    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    await inscription(req, res).catch((err) => assertEquals("La fonction devrait être redirigée vers la charte", "/charte", err));
    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    req.body.user = "admin";
    await connexion(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être connecté", "/", err);
    });

    req.body.user = "robert";
    req.body.id = "robert"
    await ajoutUtilisateur(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé, car l'utilisateur est déjà dans la base de donnée", "/users", err);
        assertEquals("Il devrait y avoir le message d'insertion reussie", `L'utilisateur ${req.body.id} a été ajouté !`, req.session.userAjoute);
    });
    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    await inscription(req, res).catch((err) => assertEquals("La fonction devrait être redirigée vers la charte", "/charte", err));
    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    req.body.user = "admin";
    await connexion(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être connecté", "/", err);
    });

    req.body.user = "norbert";
    req.body.id = "norbert"
    await ajoutUtilisateur(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé, car l'utilisateur est déjà dans la base de donnée", "/users", err);
        assertEquals("Il devrait y avoir le message d'insertion reussie", `L'utilisateur ${req.body.id} a été ajouté !`, req.session.userAjoute);
    });
    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    await inscription(req, res).catch((err) => assertEquals("La fonction devrait être redirigée vers la charte", "/charte", err));

    //----------------------------------------------------------------------------------------------------

    req.params.user = "norbert";
    await afficherProfil(req, res).catch((err) => {
        assertEquals("L'utilisateur doit accéder au profil de norbert", "user.pug", err);
    });

    req.params.user = "admin";
    await afficherProfil(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/users", err);
    });

    req.params.user = "hubert";
    await afficherProfil(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/users", err);
    });

    await pageAjoutCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/", err);
    });

    await pageModifCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/", err);
    });

    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });

    req.body.user = "admin";
    await connexion(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être connecté", "/", err);
    });

    // Ajout de cours ---------------------------------------------------------------------
    await pageAjoutCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit accéder à la page", "ajoutCours.pug", err);
    });

    await pageModifCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit accéder à la page", "modifCours.pug", err);
    });

    req.body.nom = "Maths";
    req.body.profs = "salut";
    req.body.formation = "hey";
    req.body.semestre = "7";
    req.body.desc = "court";
    await creationCours(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/", err);
    });

    req.body.formation = "2: formation non existante";
    await creationCours(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/", err);
    });

    req.body.formation = "1: formation qui existe";
    await creationCours(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/", err);
    });

    req.body.semestre = "2";
    await creationCours(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/cours/ajouter", err);
    });
    let reqe = await requete("SELECT * FROM Cours");
    assertEquals("Le nom du cours n'est pas correct", "Maths", reqe[0].nom);
    assertEquals("L'idFormation du cours n'est pas correct", 1, reqe[0].idformation);
    assertEquals("L'idSemestre du cours n'est pas correct", 2, reqe[0].idsemestre);
    assertEquals("Le nom de la description n'est pas correct", "court", reqe[0].description);
    reqe = await requete("SELECT * FROM Assurer_Cours");
    assertEquals("Aucun professeur ne devrait avoir été ajouté", 0, reqe.length);

    req.body.nom = "Francais";
    req.body.profs = "91: Jean Valjean";
    await creationCours(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/cours/ajouter", err);
    });
    reqe = await requete("SELECT * FROM Cours");
    assertEquals("Le nom du cours n'est pas correct", "Francais", reqe[1].nom);
    assertEquals("L'idFormation du cours n'est pas correct", 1, reqe[1].idformation);
    assertEquals("L'idSemestre du cours n'est pas correct", 2, reqe[1].idsemestre);
    assertEquals("Le nom de la description n'est pas correct", "court", reqe[1].description);
    reqe = await requete("SELECT * FROM Assurer_Cours");
    assertEquals("Aucun professeur ne devrait avoir été ajouté", 0, reqe.length);

    req.body.nom = "Philosophie";
    req.body.profs = "6: Professeur existant";
    await creationCours(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/cours/ajouter", err);
    });
    reqe = await requete("SELECT * FROM Cours");
    assertEquals("Le nom du cours n'est pas correct", "Philosophie", reqe[2].nom);
    assertEquals("L'idFormation du cours n'est pas correct", 1, reqe[2].idformation);
    assertEquals("L'idSemestre du cours n'est pas correct", 2, reqe[2].idsemestre);
    assertEquals("Le nom de la description n'est pas correct", "court", reqe[2].description);
    reqe = await requete("SELECT * FROM Assurer_Cours");
    assertEquals("Le professeur doit avoir été ajouté", 2, reqe[0].idcours);
    assertEquals("Le professeur doit avoir été ajouté", 6, reqe[0].idprof);

    req.body.nom = "Histoire-Géographie";
    req.body.profs = "1: Professeur existant";
    await creationCours(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/cours/ajouter", err);
    });
    reqe = await requete("SELECT * FROM Cours");
    assertEquals("Le nom du cours n'est pas correct", "Histoire-Géographie", reqe[3].nom);
    assertEquals("L'idFormation du cours n'est pas correct", 1, reqe[3].idformation);
    assertEquals("L'idSemestre du cours n'est pas correct", 2, reqe[3].idsemestre);
    assertEquals("Le nom de la description n'est pas correct", "court", reqe[3].description);
    reqe = await requete("SELECT * FROM Assurer_Cours");
    assertEquals("Le professeur doit avoir été ajouté", 3, reqe[1].idcours);
    assertEquals("Le professeur doit avoir été ajouté", 1, reqe[1].idprof);

    // TODO Faire les tests de suppression quand il y aura des cours dedans

    await formation(req, res).catch((err) => {
        assertEquals("L'utilisateur ne devrait pas être redirigé", "cours.pug", err);
    });

    req.params.formation = "4";
    await semestre(req, res).catch((err) => {
        assertEquals("L'utilisateur ne devrait pas être redirigé", "semestre.pug", err);
    });

    req.params.formation = "1";
    await semestre(req, res).catch((err) => {
        assertEquals("L'utilisateur ne devrait pas être redirigé", "semestre.pug", err);
    });
    
    req.params.formation = "4";
    req.params.semestre = "6";
    await modules(req, res).catch((err) => {
        assertEquals("L'utilisateur ne devrait pas être redirigé", "modules.pug", err);
    });

    req.params.formation = "1";
    req.params.semestre = "7";
    await modules(req, res).catch((err) => {
        assertEquals("L'utilisateur ne devrait pas être redirigé", "modules.pug", err);
    });

    req.params.formation = "1";
    req.params.semestre = "2";
    await modules(req, res).catch((err) => {
        assertEquals("L'utilisateur ne devrait pas être redirigé", "modules.pug", err);
    });

    req.params.cours = "34";
    await afficherCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/cours", err);
    });

    req.params.cours = "1";
    await afficherCours(req, res).catch((err) => {
        assertEquals("L'utilisateur ne doit pas être redirigé", "pageCours.pug", err);
    });

    req.params.cours = "34";
    await pageAjoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/cours", err);
    });

    req.params.cours = "1";
    await pageAjoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur ne doit pas être redirigé", "ajoutFichier.pug", err);
    })

    req.body.titre = "readme";
    req.body.type = "Exercices";
    req.body.desc = "description sympa";
    req.files.contenu = {};
    req.files.contenu.name = "README.md";
    req.files.contenu.size = 940;
    req.files.contenu.data = await fs.readFileSync(`./${req.files.contenu.name}`);
    await ajoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
    });

    req.files.contenu.size = 940000000;
    await ajoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/ajoutFichier/${req.params.cours}`, err);
        assertEquals("tropGros doit être déclaré", `Le fichier que vous voulez envoyer est trop gros (il fait ${(Math.round((req.files.contenu.size)/10000)/100)}Mo alors que la limite est de ${(5.0/1000000)}Mo).`, req.session.tropGros);
    });

    req.body.titre = "bash";
    req.body.desc = "description sympa 2";
    req.files.contenu.name = "lancerTests";
    req.files.contenu.size = 940;
    req.files.contenu.data = await fs.readFileSync(`./public/tests/${req.files.contenu.name}`);
    await ajoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été uploadé et sans extension", true, fs.existsSync("../charlinfo-db/cours/1/1"));
    });

    req.body.titre = "bash";
    req.body.desc = "description sympa 3";
    req.files.contenu.name = "lancerTests";
    req.files.contenu.size = 940;
    req.files.contenu.data = await fs.readFileSync(`./public/tests/${req.files.contenu.name}`);
    await ajoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été uploadé et sans extension", true, fs.existsSync("../charlinfo-db/cours/1/2"));
    });

    req.body.titre = "bash";
    req.body.desc = "description sympa 4";
    req.files.contenu.name = "lancerTests";
    req.files.contenu.size = 940;
    req.files.contenu.data = await fs.readFileSync(`./public/tests/${req.files.contenu.name}`);
    await ajoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été uploadé et sans extension", true, fs.existsSync("../charlinfo-db/cours/1/3"));
    });

    req.params.cours = "0";

    req.body.titre = "bash";
    req.body.desc = "description sympa";
    req.files.contenu.name = "lancerTests";
    req.files.contenu.size = 940;
    req.files.contenu.data = await fs.readFileSync(`./public/tests/${req.files.contenu.name}`);
    await ajoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été uploadé et sans extension", true, fs.existsSync(`../charlinfo-db/cours/${req.params.cours}/0`));
    });

    req.body.titre = "readme";
    req.body.desc = "description sympa";
    req.files.contenu.name = "README.md";
    req.files.contenu.size = 940;
    req.files.contenu.data = await fs.readFileSync(`./${req.files.contenu.name}`);
    await ajoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été uploadé et sans extension", true, fs.existsSync(`../charlinfo-db/cours/${req.params.cours}/1.md`));
    });

    req.params.cours = "0";

    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });

    req.body.user = "robert";
    await connexion(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être connecté", "/", err);
    });

    req.params.cours = "0";

    req.body.titre = "bash";
    req.body.desc = "description sympa";
    req.files.contenu.name = "lancerTests";
    req.files.contenu.size = 940;
    req.files.contenu.data = await fs.readFileSync(`./public/tests/${req.files.contenu.name}`);
    await ajoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été uploadé et sans extension", true, fs.existsSync(`../charlinfo-db/cours/${req.params.cours}/2`));
    });

    req.body.titre = "readme";
    req.body.desc = "description sympa";
    req.files.contenu.name = "README.md";
    req.files.contenu.size = 940;
    req.files.contenu.data = await fs.readFileSync(`./${req.files.contenu.name}`);
    await ajoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été uploadé et sans extension", true, fs.existsSync(`../charlinfo-db/cours/${req.params.cours}/3.md`));
    });

    req.params.cours = "2";
    req.body.titre = "readme";
    req.body.desc = "description sympa";
    req.files.contenu.name = "README.md";
    req.files.contenu.size = 940;
    req.files.contenu.data = await fs.readFileSync(`./${req.files.contenu.name}`);
    await ajoutFichierCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été uploadé et sans extension", true, fs.existsSync(`../charlinfo-db/cours/${req.params.cours}/0.md`));
    });

    // SUPPRESSION DES FICHIERS
    req.params.fichSuppr = "0";
    await suppressionFichier(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été supprimé", false, fs.existsSync(`../charlinfo-db/cours/${req.params.cours}/0.md`));
    });

    req.params.cours = "0";
    req.params.fichSuppr = "0";
    await suppressionFichier(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier ne doit pas avoir été supprimé", true, fs.existsSync(`../charlinfo-db/cours/${req.params.cours}/0`));
    });

    req.params.fichSuppr = "1";
    await suppressionFichier(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier ne doit pas avoir été supprimé", true, fs.existsSync(`../charlinfo-db/cours/${req.params.cours}/1.md`));
    });

    req.params.fichSuppr = "2";
    await suppressionFichier(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été supprimé", false, fs.existsSync(`../../../charlinfo-db/cours/${req.params.cours}/2`));
    });

    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });

    req.body.user = "admin";
    await connexion(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être connecté", "/", err);
    });

    req.params.fichSuppr = "3";
    await suppressionFichier(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été supprimé", false, fs.existsSync(`../../../charlinfo-db/cours/${req.params.cours}/3.md`));
    });

    req.params.fichSuppr = "0";
    await suppressionFichier(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", `/cours/afficher/${req.params.cours}`, err);
        assertEquals("Le fichier doit avoir été supprimé", false, fs.existsSync(`../../../charlinfo-db/cours/${req.params.cours}/0`));
    });

    // Supprimer un cours
    req.params.coursSuppr = "6";
    await supprimerCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/cours/modifier", err);
    })
    reqe = await requete("SELECT Count(*) FROM Cours");
    assertEquals("Il doit rester 4 cours", "4", reqe[0].count);

    req.params.coursSuppr = "3";
    await supprimerCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/cours/modifier", err);
    })
    reqe = await requete("SELECT id FROM Cours WHERE id = 3");
    assertEquals("Le cours devrait être supprimé", 0, reqe.length);
    reqe = await requete("SELECT id FROM Cours WHERE id = 2");
    assertEquals("Le cours devrait être supprimé", 1, reqe.length);
    reqe = await requete("SELECT id FROM Cours WHERE id = 1");
    assertEquals("Le cours devrait être supprimé", 1, reqe.length);
    reqe = await requete("SELECT id FROM Cours WHERE id = 0");
    assertEquals("Le cours devrait être supprimé", 1, reqe.length);

    req.params.coursSuppr = "1";
    await supprimerCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/cours/modifier", err);
    })
    reqe = await requete("SELECT id FROM Cours WHERE id = 2");
    assertEquals("Le cours devrait être supprimé", 1, reqe.length);
    reqe = await requete("SELECT id FROM Cours WHERE id = 1");
    assertEquals("Le cours devrait être supprimé", 0, reqe.length);
    reqe = await requete("SELECT id FROM Cours WHERE id = 0");
    assertEquals("Le cours devrait être supprimé", 1, reqe.length);

    req.body.nom = "Musique";
    req.body.profs = "12: prof";
    req.body.formation = "1";
    req.body.semestre = "1";
    req.body.desc = "court";
    await creationCours(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/cours/ajouter", err);
    });
    reqe = await requete("SELECT id FROM Cours WHERE id = 1");
    assertEquals("Le cours devrait être ajouté en position 1", 1, reqe.length);

    req.params.coursSuppr = "0";
    await supprimerCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/cours/modifier", err);
    })
    reqe = await requete("SELECT id FROM Cours WHERE id = 2");
    assertEquals("Le cours devrait être supprimé", 1, reqe.length);
    reqe = await requete("SELECT id FROM Cours WHERE id = 1");
    assertEquals("Le cours devrait être supprimé", 1, reqe.length);
    reqe = await requete("SELECT id FROM Cours WHERE id = 0");
    assertEquals("Le cours devrait être supprimé", 0, reqe.length);

    req.params.coursSuppr = "1";
    await supprimerCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/cours/modifier", err);
    })
    reqe = await requete("SELECT id FROM Cours WHERE id = 2");
    assertEquals("Le cours devrait être supprimé", 1, reqe.length);
    reqe = await requete("SELECT id FROM Cours WHERE id = 1");
    assertEquals("Le cours devrait être supprimé", 0, reqe.length);

    req.params.coursSuppr = "2";
    await supprimerCours(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être redirigé", "/cours/modifier", err);
    })
    reqe = await requete("SELECT id FROM Cours WHERE id = 2");
    assertEquals("Le cours devrait être supprimé", 0, reqe.length);

    // ---------------------------- Opérations sur les utilisateurs ----------------------------------

    req.params.user = "rob";
    await retrograder(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    await promouvoir(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    await bannir(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    await debannir(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    await supprimerUtilisateur(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });

    req.params.user = "robert";
    await retrograder(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT grade FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur ne devrait pas pouvoir être retrogradé en dessous de 1", 1, reqe[0].grade);

    await promouvoir(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT grade FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur devrait avoir le grade 2", 2, reqe[0].grade);

    await retrograder(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT grade FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur ne devrait avoir le grade 1", 1, reqe[0].grade);

    await bannir(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT motDePasse FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur devrait avoir le mot de passe BANNI", "BANNI", reqe[0].motdepasse);

    await debannir(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT motDePasse FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur devrait avoir aucun mot de passe", null, reqe[0].motdepasse);

    await supprimerUtilisateur(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT id FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur ne devrait plus exister", 0, reqe.length);

    // --------------------------------------------------------------------

    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    req.body.user = "hubert";
    await connexion(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être connecté", "/", err);
    });

    //--------------
    req.params.user = "norbert";
    await retrograder(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT grade FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur ne devrait pas pouvoir être retrogradé en dessous de 1", 1, reqe[0].grade);

    await promouvoir(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT grade FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur devrait avoir le grade 1", 1, reqe[0].grade);

    await bannir(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT motDePasse FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur ne devrait pas avoir changé de mdp", "1fd40af847607e105f9775a18d1c3aef325ba30b0f448352a51380f519d63516", reqe[0].motdepasse);

    await debannir(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT motDePasse FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur ne devrait pas avoir changé de mdp", "1fd40af847607e105f9775a18d1c3aef325ba30b0f448352a51380f519d63516", reqe[0].motdepasse);

    await supprimerUtilisateur(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/users", err);
    });
    reqe = await requete("SELECT id FROM Compte WHERE id = $1", [req.params.user]);
    assertEquals("L'utilisateur devrait toujours exister", 1, reqe.length);

    await pageAjoutChangelog(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/", err);
    });

    // --------------------------------------------------------------------

    await deconnexion(req, res).catch((err) => {
        assertEquals("L'utilisateur doit être déconnecté", undefined, req.session.user);
        assertEquals("La fonction devrait être redirigée", "/", err);
    });
    req.body.user = "admin";
    await connexion(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être connecté", "/", err);
    });

    //--------------

    await pageAjoutChangelog(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "changelog.pug", err);
    });

    req.body.version = "1.0";
    req.body.desc = "tests écrits>tests lancés>tests réalisés avec succès";
    await ajoutChangelog(req, res).catch((err) => {
        assertEquals("L'utilisateur devrait être redirigé", "/changelog", err);
    });
    reqe = await requete("SELECT nom, date, description FROM Changelog INNER JOIN Description_changelog on description_changelog.nomchangelog = changelog.nom ORDER BY date DESC");
    assertEquals("Il devrait y avoir 3 propriétés", 3, Object.keys(reqe[0]).length);
    assertEquals("La version devrait être 1.0", "1.0", reqe[0].nom);
    assertEquals("La description n'est pas bonne", "tests écrits", reqe[0].description);
    assertEquals("La description n'est pas bonne", "tests lancés", reqe[1].description);
    assertEquals("La description n'est pas bonne", "tests réalisés avec succès", reqe[2].description);
}

async function waitFor(time) {
    return new Promise((resolve) => {
        setTimeout(resolve, time);
    })
}
    

tests()
.then(() => {
    logcolor("Les tests ont été réalisés avec succès !", "cyan");
    process.exit(0);
})
.catch((err) => {
    logcolor("ERREUR : Il y a eu une erreur dans le programme de test...", "rouge");
    logcolor(err, "magenta");
    process.exit(1);
})

