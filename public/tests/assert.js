const {logcolor} = require("../modules/color.js")

/**
 * Fonction qui initialise un assert
 * @returns {Function} fonction assertEquals
 */
exports.assert = function(nbAppels) {
    let compte = 1;
    return (messageErreur, reponseAttendue, reponseObtenue) => {
        assertEq(messageErreur, reponseAttendue, reponseObtenue, compte++, nbAppels);
    }
}

/**
 * Fonction qui fait un test
 * @param {string} messageErreur Message d'erreur
 * @param {*} reponseAttendue Réponse attendue
 * @param {*} reponseObtenue Réponse obtenue
 * @returns {*} Quitte le programme s'il y a une erreur
 */
let assertEq = function(messageErreur, reponseAttendue, reponseObtenue, compte, nbAppels) {
    if(reponseAttendue !== reponseObtenue) {
        logcolor(`Erreur : ${messageErreur}`, "rouge");
        logcolor(`Attendu : ${reponseAttendue}`, "rouge");
        logcolor(`Obtenu : ${reponseObtenue}`, "rouge");
        process.exit(1);
    }
    if(nbAppels !== undefined) {
        logcolor(`${compte} ${Math.floor(compte/nbAppels * 100)}% : ok`, "vert");
    }
    else {
        logcolor(`${compte} : ok`, "vert");
    }
    
}