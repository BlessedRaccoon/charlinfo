-- Changer les dates en timestamp (with time zone) pour augmenter la précision des dates
ALTER TABLE Compte
ALTER COLUMN dateCreation
TYPE timestamp with time zone;

ALTER TABLE Fichier
ALTER COLUMN dateFich
TYPE timestamp with time zone;

-- Ajouter des colonnes dans Formation
ALTER TABLE Formation
ADD COLUMN description varchar;

ALTER TABLE Formation
ADD COLUMN duree integer;

UPDATE Formation SET description = 'Dans le DUT Informatique, on apprend les bases de l''Informatique et de la programmation.';
UPDATE Formation SET duree = 4;